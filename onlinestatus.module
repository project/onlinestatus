<?php
/**
 * @file
 * The .module File of the Onlinestatus Indicator.
 *
 * This file will be loaded by Drupal on demand. An enables an Onlinestatus
 * Indicators for the most commonly used Instant Messenagers today.
 *
 * This file is part of Onlinestatus Indicator.
 *
 * Onlinestatus Indicator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * Onlinestatus Indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onlinestatus Indicator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Christian Edelmann <xeniac@gmx.at>
 * @version $Revision$
 */

/**
 * Returns an associative array with all Supported Messengers with title.
 *
 * @param boolean $only_activated If set to true only this function only
 *                returns the activated Messengers.
 * @return array an associative array 'messenger' => 'Messenger Title'
 */
function onlinestatus_get_messengers($only_activated = true) {
  $supportet_messengers = array (
    'aim' => t('AOL Instant messenger'),
    'icq' => t('ICQ'),
    'gtalk' => t('Google Talk'),
    'jabber' => t('Jabber'),
    'msn' => t('MSN messenger'),
    'skype' => t('Skype'),
    'yahoo' => t('Yahoo! messenger'),
    'xfire' => t('XFire'),
  );
  if ($only_activated) {
    $return = array ();
    foreach (variable_get('onlinestatus_messengers', array ()) as $messenger) {
      $return[$messenger] = $supportet_messengers[$messenger];
    }
    return $return;
  }
  else {
    return $supportet_messengers;
  }
}

/**
 * Implementation of hook_help(), generates the Helpmessage for this Module.
 *
 * @param string $section the Drupal Path.
 * @return string
 */
function onlinestatus_help($section) {
  global $user;
  switch ($section) {
    case 'admin/help#onlinestatus' :
      return t('
        <p>This Module adds Onlinestatus Indicator support for the most common Instant Messengers into Drupal.
        The User could fill in his messengerIDs in the user account form and the onlinestatus for each messenger will be promoted on the users profile page.</p>
        <p>You can</p>
        <ul>
        <li>control which messenger should be supportet by your website in  <a href="%settingsurl">administer &gt;&gt; settings &gt;  &gt; onlinestatus</a>.</li>
        <li><a href="%messengerurl">fill in your messenger account data</a>.</li>
        <li><a href="%profileurl">see your onlinestatus</a>.</li>
        </ul>
      ',array('%settingsurl' => url('admin/settings/onlinestatus'), '%messengerurl' => url('user/'.$user->uid.'/edit'), '%profileurl' => url('user/'.$user->uid)));
    case 'admin/help#onlinestatus':
      return 'reservated for some wise words';
    case 'admin/settings/onlinestatus' :
      return t('<p>These are the Basic Settings for the Online Status Indicator.</p>');
  }
}

/**
 * A 'Meta Function' for every messenger.
 *
 * onlinestatus_messenger contains the code that is common to every messenger.
 * For the different part this function it includes messengers.inc.php
 * and calls onlinestatus_messenger_$messenger
 *
 * @param string $op possible: description, update, url, settings, validate
 * @param string $messenger the desired messenger 'jabber' for example.
 * @param string $object a drupal object that contains user information, this could be a user or author object.
 * @return string
 */
function onlinestatus_messenger($op, $messenger, $object = null) {
  include_once (drupal_get_path('module','onlinestatus').'/messengers.inc.php');
  switch ($op) {
    case 'description' :
      //returns the description text for the inputfield in the userform.
      return call_user_func('onlinestatus_messenger_'.$messenger, 'description', $object-> $messenger);
    case 'status' :
      if($object->onlinestatus_expose == 0 ) {
        return 'unknown';
      }
      $status = db_fetch_array(db_query("SELECT status,changed FROM {onlinestatus} WHERE uid=%d AND messenger='%s'", $object->uid, $messenger));
      //Check if the Online Information in the Database is older then the cachetime.
      if ((time() - $status['changed']) > variable_get('onlinestatus_cachetime', 0)) {
        $status = call_user_func('onlinestatus_messenger_'.$messenger, 'update', $object->$messenger);
        db_query("DELETE FROM {onlinestatus} WHERE uid=%d AND messenger='%s'", $object->uid, $messenger);
        db_query("INSERT INTO {onlinestatus} (uid,messenger,status,changed) VALUES (%d,'%s','%s',%d)", $object->uid, $messenger, $status, time());
      }
      else {
        $status = $status['status'];
      }
      return $status;
    case 'url' :
      $url = call_user_func('onlinestatus_messenger_'.$messenger, 'url', $object-> $messenger);
      if ($url == false) {
        $url = url('user/'.$object->uid);
      }
      return $url;
    default :
      return call_user_func('onlinestatus_messenger_'.$messenger, $op, $object);
  }
  return false;
}

function onlinestatus_menu($may_cache) {
  global $user;
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/onlinestatus', 
      'title' => t('Onlinestatus Indicator'),
      'description' => t('Configure the Status Settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('onlinestatus_admin_settings'),
      'access' => 'administer site configuration',
    );
  }
  return $items;
} 
/**
 * Implementation of hook_perm().
 */
function onlinestatus_perm() {
  return array ('access onlinestatus');
}

/**
 * Shows the General Setup for the Module
 */
function onlinestatus_admin_settings() {
  //Only a real Site Administrator should be able to change this.
  if (!user_access("administer site configuration")) {
    return message_access();
  }
  //Check if PHP has CURL Support, else drop a message.
  if (!extension_loaded('curl')) {
    drupal_set_message(t('Your PHP installation has no CURL Support, Onlinestatus will not work.'), 'error');
  }
  //General Module Settings
  $form['general'] = array (
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => true,
    '#collapsed' => false,);
    
  $form['general']['onlinestatus_messengers'] = array (
    '#type' => 'select',
    '#multiple' => true,
    '#title' => t('Use this messengers'),
    '#options' => onlinestatus_get_messengers(false),
    '#default_value' => variable_get('onlinestatus_messengers', array ('aim','icq','skype','yahoo')),
    '#description' => t('Select all messengers that you want to support.'));

  $form['general']['onlinestatus_osidescription'] = array(
    '#type' => 'markup',
    '#value' => t('<strong>This Module needs an <a href="!url">Online Status Indicator Server</a> to query the MSN and Jabber Onlinestatus.</strong>', array('!url' => url('http://onlinestatus.org/')) ).
    t('<p>Online Status Indicator is a service that lets you put a small image on a web page to show if you are online on AOL Instant Messenger, ICQ, IRC, Jabber, MSN Messenger,  and Yahoo Messenger.</p>').
    t('<p>The recommended way is to install your own Online Status Indicator Server, if you don\'t have a Server with Java support and Shell access you can use one of the servers listet on onlinestatus.org '));

  $form['general']['onlinestatus_osiserver'] = array (
    '#type' => 'textfield',
    '#title' => t('URL to your Online Status Indicator'),
    '#default_value' => variable_get('onlinestatus_osiserver', ''),
    '#description' => t('Enter the complete URL to your Online Status Indicator Service like <address>http://www.example.com:8080</address>'));

  $form['general']['onlinestatus_registration'] = array (
    '#type' => 'checkbox',
    '#title' => t('Show Onlinestatus Form on Registration'),
    '#default_value' => variable_get('onlinestatus_registration', false),
    '#description' => t('Activate this Checkbox to add the Onlinestatus fields to the registration Progress'));

  //Image Preview and Image Settings
  $form['images'] = array (
    '#type' => 'fieldset',
    '#title' => t('Image Settings'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Reload this page, to see the changes you have made.'));

  $form['images']['onlinestatus_imagepath'] = array (
    '#type' => 'textfield',
    '#title' => t('Image Path'),
    '#default_value' => variable_get('onlinestatus_imagepath', '%module/images/%im-%status.png'),
    '#description' => t('Path for the inidcator Images.').'<br />'.t('%im will be substituted with aim,icq,jabber, and so on.<br/>%theme will be substituted with the path to your current Theme<br/> %status will be substituted with online, offline, or unknown<br/>%module ist the current Module Path'));

  $preview = '<div>';
  foreach (onlinestatus_get_messengers() as $messenger => $title) {
  	$preview .= "<strong>$messenger:</strong> ";
    foreach (array('unknown','online','offline') as $status) {
      $image = strtr(variable_get('onlinestatus_imagepath', ''),
        array (
          '%im' => $messenger,
          '%status' => $status,
          '%theme' => path_to_theme(),
          '%module' => drupal_get_path('module', 'onlinestatus')
        )
      );
      $preview .= theme('image', $image, $messenger, $status, array('class' => 'onlinestatus-indicator'), FALSE);
    }
    $preview .= '<br/>';
  }
  $output .= '</div>';
  $form['images']['preview'] = array ('#value' => $preview);

  $form['performance'] = array (
    '#type' => 'fieldset',
    '#title' => t('Performance Settings'),
    '#collapsible' => true,
    '#collapsed' => true);

  $form['performance']['help'] = array (
    '#type' => 'markup',
    '#value' => t('<p>Changing this Settings will affect the performance of your website.</p>'));

  $form['performance']['onlinestatus_cachetime'] = array (
    '#type' => 'textfield',
    '#title' => t('Cache Onlinestatus'),
    '#default_value' => variable_get('onlinestatus_cachetime', '60'),
    '#description' => t('Cache the Onlinestatus for x Seconds.'));

  $form['performance']['onlinestatus_timeout'] = array (
    '#type' => 'textfield',
    '#title' => t('Query Timeout'),
    '#default_value' => variable_get('onlinestatus_timeout', '3'),
    '#description' => t('The Maximum Limit an Statusquery should take'));


  //add messenger specific options:
  foreach (onlinestatus_get_messengers(false) as $messenger => $title) {
    $settings = onlinestatus_messenger('settings', $messenger);
    if (is_array($settings)) {
      $form[$messenger] = array (
        '#type' => 'fieldset',
        '#title' => $title,
        '#collapsible' => true,
        '#collapsed' => true,);
      $form[$messenger][] = $settings;
    }
  }
  return system_settings_form($form);
}

/**
 * Implementation of hook_user().
 */
function onlinestatus_user($op, & $edit, & $user, $category = NULL) {
  switch ($op) {
    case 'delete' :
    case 'load' :
    case 'login' :
    case 'logout' :
      break;
    case 'form' :
      if ($category == 'account') {
        return onlinestatus_user_form($edit, $user, $category);
      }
      break;
    case 'insert' :
      return onlinestatus_user_update($edit, $user, $category);
    case 'register' :
      if (variable_get('onlinestatus_registration',false)) {
          return onlinestatus_user_form($edit, $user, $category);
      }
    case 'update' :
      return onlinestatus_user_update($edit, $user, $category);
    case 'validate' :
      return onlinestatus_user_validate($edit, $category);
    case 'view' :
      return theme('onlinestatus_profile', $user);
  }
}

/**
 * Generates the Onlinestatus Section in the user/edit form.
 */
function onlinestatus_user_form($edit, $user, $category) {
  $form['onlinestatus'] = array (
    '#type' => 'fieldset',
    '#title' => t('Instant Messengers'),
    '#collapsible' => TRUE, '#collapsed' => false,
    '#description' => t('Here you can input you Accounts of all Instant messengers that you use.'),
    '#weight' => 3);
  foreach (onlinestatus_get_messengers(true) as $messenger => $title) {
    $form['onlinestatus'][$messenger] = array (
      '#type' => 'textfield',
      '#title' => "$title",
      '#default_value' => $user->$messenger,
      '#description' => onlinestatus_messenger('description', $messenger),
     );
  }
  if (!isset($user->onlinestatus_expose)) {
  	$user->onlinestatus_expose = true;
  }
  $form['onlinestatus']['onlinestatus_expose'] = array (
    '#type' => 'checkbox',
    '#title' => t('Expose my Onlinestatus'),
    '#default_value' => $user->onlinestatus_expose,
    '#description' => t('Activate this Checkbox if you want to expose your online status to other users'),
   );

  return $form;
}

/**
 * deletes the status cache on a user update.
 */
function onlinestatus_user_update($edit, $user, $category) {
  db_query("DELETE FROM {onlinestatus} WHERE uid = %d", $user->uid);
}


/**
 * Validates the account information for each messenger.
 */
function onlinestatus_user_validate($edit, $category) {
  foreach (onlinestatus_get_messengers() as $messenger => $title) {
    if (!empty ($edit[$messenger]) && !onlinestatus_messenger('validate', $messenger, $edit[$messenger])) {
      form_set_error(
        $messenger,
        t('%value is not a valid Account for %title',
        array ('%value' => $edit[$messenger], '%title' => $title))
      );
    }
  }
}


/**
 * Displays the Onlinestatus of all messengers on the user profile page.
 */
function theme_onlinestatus_profile($user) {
  $items = array();
  drupal_add_css(drupal_get_path('module', 'onlinestatus') .'/onlinestatus.css');
  foreach (onlinestatus_get_messengers(true) as $messenger => $title) {
    if (empty ($user->$messenger)) {
      continue;
    }
    $status = onlinestatus_messenger('status', $messenger, $user);
    $image = theme('onlinestatus_indicator', $user, $messenger);
    $url = onlinestatus_messenger('url', $messenger, $user);
    $items[] = array(
      'value' => sprintf('%s  <a href="%s">%s (%s)</a>', $image, $url, $user->$messenger, $status),
    );
  }
  return array(t('Online Status') => $items);
}


/**
 * Themeable Statusindicator for Drupal.
 *
 * Generates the indicator image.
 * Call this function with theme('onlinestatus_inidicator',$user, $messenger)
 *
 * @see theme()
 * @param object $user can be an node, comment, or user object object.
 * @param string $messenger the Name of the Messenger 'aim' for example.
 * @return string
 */
function theme_onlinestatus_indicator($user, $messenger) {
  $status = onlinestatus_messenger('status', $messenger, $user);
  $url = onlinestatus_messenger('url', $messenger, $user);
  $image = strtr(
    variable_get('onlinestatus_imagepath', ''),
    array (
      '%im' => $messenger, 
      '%status' => $status, 
      '%theme' => path_to_theme(), 
      '%module' => drupal_get_path('module', 'onlinestatus'),
    )
  );
  return sprintf(
    '<a href="%s">%s</a>',
    onlinestatus_messenger('url',$messenger, $user),
    theme('image', $image, $messenger, $status, array('class' => 'onlinestatus-indicator'), FALSE)
  );
}


/**
 * fetches the Data of an url via CURL
 *
 * @var handle $curl_connection the handle for the CURL Connection
 * @param string $url the url we want to see
 * @param boolean $fetch_header set to true if you need the http Header.
 * @param boolean $no_content set to true if you don't need the HTML Content.
 * @param boolean $foolw_location set to true if you want to CURL to follow http redirects.
 * @return string
 **/
function onlinestatus_fetch_url($url, $fetch_header = true, $no_content = true, $follow_location = false) {
  $curl_connection = curl_init($url);
  curl_setopt($curl_connection, CURLOPT_TIMEOUT, variable_get('onlinestatus_timeout', 3));
  curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_connection, CURLOPT_HEADER, $fetch_header);
  curl_setopt($curl_connection, CURLOPT_NOBODY, $no_content);
  curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, $follow_location);
  $data = curl_exec($curl_connection);
  if (curl_errno($curl_connection)) {
    watchdog('onlinestatus', curl_error($curl_connection), WATCHDOG_NOTICE, $url);
  }
  curl_close($curl_connection);
  return trim($data);
}
?>
