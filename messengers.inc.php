<?php
/**
 * @file
 * Messenger support for the Onlinestatus Indicator.
 *
 * This file will be loaded by onlinestatus_messenger() and adds support for the
 * different Instant Messenger types.
 *
 * This file is part of Onlinestatus Indicator.
 *
 * Onlinestatus Indicator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * Onlinestatus Indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onlinestatus Indicator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Christian Edelmann <xeniac@gmx.at>
 * @version $Revision$
 */

/**
 * Onlinestatus Indicator support for Yahoo! Messenger
 *
 * @param string $op possible operations: update, url, validate
 * @param string $account AIM Screenname
 * @return string
 */
function onlinestatus_messenger_aim($op, $account) {
  switch ($op) {
    case 'update' :
      $url = sprintf('http://big.oscar.aol.com/%s?on_url=online&off_url=offline', $account);
      $data = onlinestatus_fetch_url($url, true, false);
      if (strchr($data, 'online')) {
        return 'online';
      }
      elseif (strchr($data, 'offline')) {
        return 'offline';
      }
      else {
        return 'unknown';
      }
    case 'url' :
      return sprintf('aim:GoIM?screenname=%s', $account);
    case 'validate' :
      if (preg_match('/^([a-zA-Z][a-zA-Z0-9]{3,16}(@mac\.com)?)$/', $account)) {
        return true;
      }
      else {
        return false;
      }
  }
}


/**
 * Onlinestatus Indicator support for Google Talk
 *
 * @param string $op possible operations: description, update, url, settings, validate
 * @param string $account the gmail.com mail address
 * @return string
 */
function onlinestatus_messenger_gtalk($op, $account = null) {
  switch ($op) {
    case 'description' :
      return t('Support for GoogleTalk is very prelimanary at the Moment, you get the best Results with the PSI Messenger.').'<br />'. onlinestatus_messenger_jabber('description');
    case 'update' :
      return onlinestatus_messenger_jabber($op,"$account@gmail.com");
    case 'url' :
      return sprintf('xmpp:%s', "$account@gmail.com");
    case 'settings' :
      $return['onlinestatus_gtalk'] = array (
        '#type' => 'markup',
        '#value' => '<div>'.t('At this Point we handle GoogleTalk as an Jabber Account on gmail.com. Unforunely it seems that the GoogleTalk Client does not support status notification.').'</div>');
      return $return;
    case 'validate' :
      if (valid_email_address("$account@gmail.com")) {
        return true;
      }
      else {
        return false;
      }
  }
}


/**
 * Onlinestatus Indicator support for ICQ
 *
 * @param string $op possible operations: update, url, validate
 * @param string $account the Yahoo! Account
 * @return string
 */
function onlinestatus_messenger_icq($op, $account = null) {
  switch ($op) {
    case 'update' :
      $url = sprintf('http://status.icq.com/online.gif?icq=%s&img=1', $account);
      $data = onlinestatus_fetch_url($url, true, true);
      if (strstr($data, 'online0') != false) {
        $status = 'offline';
      }
      elseif (strstr($data, 'online1') != false) {
        $status = 'online';
      }
      else {
        $status = 'unknown';
      }
      return $status;
    case url :
      return sprintf('http://www.icq.com/whitepages/wwp.php?to=%d', $account);
    case 'validate' :
      if (preg_match('/^\d*$/', $account) && strlen($account) <= 32) {
        return true;
      }
      else {
        return false;
      }
  }
}

/**
 * Onlinestatus Indicator support for Jabber
 *
 * @param string $op possible operations: description, update, settings, url, validate
 * @param string $account the Jabber ID
 * @return string
 */
function onlinestatus_messenger_jabber($op, $account = null) {
  switch ($op) {
    case 'description' :
      return t('For Jabber Onlinestatus, please Allow user %jabber to see your online status.', array ('%jabber' => variable_get('onlinestatus_jabberuser', 'onlinestatus@jabber.org')));
    case 'update' :
      //Query Edgar
      if (variable_get('onlinestatus_edgar', true)) {
        $url = sprintf('%s/status.php?jid=%s&type=text', variable_get('onlinestatus_edgarserver', 'http://edgar.netflint.net'), $account);
        $data = strtok(onlinestatus_fetch_url($url, false, false), ':');
        switch ($data) {
          case 'Online' :
          case 'Free for chat' :
          case 'Away' :
          case 'Not Available' :
          case 'Do not disturb' :
            return 'online';
          case 'Offline' :
            return 'offline';
          default :
            return 'unknown';
        }
      //Query Onlinestaus.org
      } else {
        $url = sprintf('%s/jabber/%s', variable_get('onlinestatus_osiserver', ''), urlencode($account));
        $data = onlinestatus_fetch_url($url, true, false);
        if (strchr($data, 'online')) {
          return 'online';
        }
        elseif (strchr($data, 'offline')) {
          return 'offline';
        }
        else {
          return 'unknown';
        }
      }
    case 'url' :
      return sprintf('xmpp:%s', $account);
    case 'settings' :
      $return['onlinestatus_jabberuser'] = array ('#type' => 'textfield', '#title' => t('Account of your Jabber Bot'), '#default_value' => variable_get('onlinestatus_jabberuser', ''), '#description' => t('The Online Status Indicator Service needs an Bot to fetch the Status Information from Jabber users. Your Users must add this Bot to their Buddylist.<br/> You can find the account of the jabber bot on your <a href="!url">onlinestatus.org server</a>',array('!url' => variable_get('onlinestatus_osiserver','').'/register?type=jabber&name=user@example.com&onurl=&offurl=&unknownurl=&ircchannel=')),);
      $return['onlinestatus_edgar'] = array ('#type' => 'checkbox', '#title' => t('Use Edgar'), '#default_value' => variable_get('onlinestatus_edgar', 'false'), '#description' => t('Use an <a href="!edgar">Edgar Server</a> instead of the <a href="!osi">Onlinestatus.org</a> Server.', array ('!edgar' => 'http://edgar.netflint.net', '!osi' => 'http://onlinestatus.org')));
      $return['onlinestatus_edgarserver'] = array ('#type' => 'textfield', '#title' => t('Edgar Server'), '#default_value' => variable_get('onlinestatus_edgarserver', 'http://edgar.netflint.net'), '#description' => t("If you want to use <a href=\"!url\">Edgar</a> for Jabber, enter the URL to your Edgar Server.", array ('!url' => 'http://edgar.netflint.net')));
      return $return;
    case 'validate' :
      if (valid_email_address($account)) {
        return true;
      }
      else {
        return false;
      }
  }
}


/**
 * Onlinestatus Indicator support for MSN Messenger
 *
 * @param string $op possible operations: description, update, url, validate
 * @param string $account the .net Passport Login
 * @return string
 */
function onlinestatus_messenger_msn($op, $account = null) {
  switch ($op) {
    case 'description' :
      return t('Please check your Privacy settings in MSN Messenger. Make sure &quot;All other users&quot; is in your Allow list.');
    case 'update' :
      $url = sprintf('%s/msn/%s',variable_get('onlinestatus_osiserver', ''), $account);
      $date = onlinestatus_fetch_url($url, true, false, false);
      if (strstr($data, 'online')) {
        return 'online';
      }
      elseif (strstr($data, 'offline')) {
        return 'offline';
      }
      else {
        return 'unknown';
      }
    case 'url' :
        return '#';
    case 'validate':
      if (valid_email_address($account)) {
        return true;
      }
      else {
        return false;
      }
  }
}


/**
 * Onlinestatus Indicator support for Skype
 *
 * @param string $op possible operations: description, update, url, validate
 * @param string $account the Skype Username
 * @return string
 */
function onlinestatus_messenger_skype($op, $account = null) {
  switch ($op) {
    case 'description' :
      return t('You need at least Skype 2.0.0.81 for this Feature. If you have problemes open your privacy Settings and check the "Allow Status to be shown on the Web" option.');
    case 'update' :
      $url = sprintf('http://mystatus.skype.com/%s.txt', $account);
      $status = onlinestatus_fetch_url($url, false, false);
      switch ($status) {
        case 'Online' : //online
        case 'Skype Me' : //skype me
        case 'Away' : //away
        case 'Not Available' : //not available
        case 'Do Not Disturb' : //dnd
        case 'e167636c89d10c794bc4a96533a32f25' : //On a Call
          return 'online';
        case 'Invisible' : //Invisible
        case 'Offline' : //offline
          return 'offline';
        default :
          return 'unknown';
      }
    case 'url' :
      return sprintf('callto://%s', $account);
    case 'validate' :
      if (preg_match('/\b^[a-zA-Z][a-zA-Z_\d]{3,15}/', $account) && strlen($account) <= 32) {
        return true;
      } else {
        return false;
      }
  }
}


/**
 * Onlinestatus Indicator support for Xfire
 *
 * @param string $op possible operations: description, update, url, validate
 * @param string $account the Skype Username
 * @return string
 */
function onlinestatus_messenger_xfire($op, $account) {
  switch ($op) {
    case 'description' :
      return false;
    case 'update' :
      $url = sprintf('http://www.xfire.com/profile/%s/', $account);
      $data = onlinestatus_fetch_url($url, false, false);
      if (strstr($data, 'class="offline"><b>offline</b></span>')) {
          return 'offline';
      } elseif (strstr($data, 'class="online"><b>online</b></span>')) {
          return 'online';
      } else {
          return 'unknown';
      }
    case 'url' :
      return sprintf('http://www.xfire.com/profile/%s/', $account);
    case 'validate' :
      if (preg_match('/\b^[a-zA-Z\d]{1,25}/', $account) && strlen($account) <= 16) {
        return true;
      }
      else {
        return false;
      }
  }
}

/**
 * Onlinestatus Indicator support for Yahoo! Messenger
 *
 * @param string $op possible operations: description, update, url, validate
 * @param string $account the Yahoo! Account
 * @return string
 */
function onlinestatus_messenger_yahoo($op , $account = null) {
  switch ($op) {
    case 'description' :
      return t('Edit your <a href="!url">Yahoo profile</a> and uncheck the <em>&ldquo;Check the box to hide my online status from other users&rdquo;</em> box.', array ('!url' => 'http://login.yahoo.com/config/edit_identity?.src=prf&.done=http://messenger.yahoo.com/'));
    case 'update' :
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_TIMEOUT, variable_get('onlinestatus_timeout', 3));
      curl_setopt($ch, CURLOPT_URL, sprintf('http://opi.yahoo.com/online?u=%s&m=t', $account));
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      if (curl_errno($ch)) {
        error_handler(curl_errno($ch), curl_error($ch), __FILE__, __LINE__);
      }
      $data = curl_exec($ch);
      curl_close($ch);
      if (strchr($data, 'NOT ONLINE') == false) {
        $status = 'online';
      }
      else {
        $status = 'offline';
      }
      unset ($ch, $data);
      return $status;
    case 'url' :
      return sprintf('http://edit.yahoo.com/config/send_webmesg?.target=%s&.src=pg', $account);
    case 'validate' :
      if (preg_match('/\b[a-zA-Z\d_]{2,32}/', $account) && strlen($account) <= 32) {
        return true;
      }
      else {
        return false;
      }
  }
}
?>
